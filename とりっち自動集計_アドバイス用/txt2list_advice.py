# coding: utf-8

# -------------
# HTML list code generator
# Usage : Drag & Drop a text file

import sys
import os

header = \
    '<!doctype html>\n'\
    '<html lang="ja">\n'\
    '<head>\n'\
    '<meta charset="UTF-8">\n'\
    '<title>txt2list_advice</title>\n'\
    '</head>\n'\
    '<body>\n'\
    '  <ul>\n'

footer = \
    '  </ul>\n'\
    '</body>\n'\
    '</html>'

def txt2list_advice( InFile ):

    f = open( InFile, 'r')
    lines = f.readlines()
    f.close()

    filename = os.path.splitext( InFile )[0]
    OutFile = filename + '.html'
    # print( OutFile )
    f = open( OutFile, 'w')

    # Write html header
    f.write( header )

    cnt = 0
    for line in lines:
        text = line.replace('\n', '')
        text = text.replace('\r', '')   # for windows

        if( len(text) > 0 ):
            cnt += 1
            print( '[' + str(cnt) + '] ' + text)
            list = text.split(' (by ')
            text_ad = list[0]
            text_by = (list[1])[:-1]
            print( text_ad + "  :  " + text_by )

            f.write(  '    <li><span class="advice">' + text_ad + '</span>'\
                              '<span class="name">(by ' + text_by + ')</span></li>\n')

    # Wrtie html footer
    f.write( footer )

    f.close()

if __name__ == '__main__':
    args = sys.argv
    if 1 < len(args):
        files = args
        for file in files:
            if ( os.path.isfile( file ) ):
                filename, ext = os.path.splitext( file )
                if( ext == '.txt' ):
                    print( 'Input file --> ' + str( file ) )
                    # Call main function
                    txt2list_advice( file )
            else:
                print('[ERROR] 対象のファイルが存在しません\n')
    else:
        print('[ERROR] テキストファイルを引数として入力してください\n')
        print('< 使い方 >')
        print('python txt2list_advice.py sample.txt\n')
        print('< もしくは (全てのテキストファイルが対象) >')
        print('python txt2list_advice.py *\n')
