# coding:utf-8

import os
import sys
import csv

OUT_FILE = "output.csv"         # CSV file name (output)

# ---------------
#  Main
# ---------------
if __name__ == '__main__':
    args = sys.argv

    if( len(args) < 2 ):
        print( "Error" )
    else:
        INFILE = args[1]
        # INFILE = "鳥さん系YouTubeチャンネル人気投票 _決定版.csv"

        #,"当アンケートをどこで知りましたか？","あなたの性別は？","あなたの年代は？","ニックネーム"
        # ,"amazonギフト券の連絡先","自由な通信欄"

        list = []

        # Read CSV data
        with open(INFILE, encoding="utf_8_sig") as f:
            reader = csv.reader(f)
            for row in reader:
                # Personal data
                what = row[11]
                sex  = row[12]
                age  = row[13]
                name = row[14]
                mail = row[15]      # @付きの人は excel で開くと NAME となるので何か対応する
                free = row[16].replace('\n', '').replace('\r', '')

                for i in range(5):
                    ch = row[1 + i * 2]
                    cm = row[1 + i * 2 + 1].replace('\n', '').replace('\r', '')
                    if( len(ch) ):
                        list.append( [ch, cm, age, sex, name, what, mail, free] )
        # print( list )

        # Write updated CSV data
        with open(OUT_FILE, 'w', encoding="utf_8_sig") as f:
            writer = csv.writer(f, lineterminator='\n')
            for i in list:
                writer.writerow( i )

        # Display list data
        cnt = 0
        for i in list:
            cnt += 1
            print( str(cnt) + " : " +  str(i) )
