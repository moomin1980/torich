# coding:utf-8

import os
import sys
import csv
import re

OUT_FILE = "output.csv"         # CSV file name (output)
OUT_DIR  = "output/"
DISP_NUM = 1

header = \
    '<div class="comment">\n'\
    '  <ul class="accordion">\n'\
    '    <li>\n'\
    '      <ul class="inner_open">\n'\

footer = \
    '      </ul>\n'\
    '    </li>\n'\
    '  </ul><!-- accordion -->\n'\
    '</div>'

def export_html( ch, list ):
    os.makedirs( OUT_DIR, exist_ok=True)
    # 禁止文字を削除
    ch = re.sub(r'[\\/:*?"<>|]+','',ch)
    filename = OUT_DIR + ch + ".html"
    print( filename )

    f = open( filename, 'w', encoding="utf_8_sig")
    # # Write html header
    f.write( header )

    body = \
        '      </ul>\n'\
        '    </li>\n'\
        '    <li>\n'\
        '      <p class="more"><span class="c_open">推薦コメントを見る（'+ str( len(list) - DISP_NUM) + '件）</span><span class="c_close">推薦コメントを閉じる</span></p>\n'\
        '      <ul class="inner">\n'\

    cnt = 0
    for li in list:
        ch, id, rank, cm, name, sex, age = li
        if( name == "" ) :
            name = "匿名さん"
        if( sex != "" ):
            sex = '/' + sex
        if( age != "" ):
            age = '/' + age
        code = '        <li><span class="text">' + cm + '</span><span class="info">（' + name + sex + age + '）</span></li>\n'
        # 初めて見た鳥系youtubeでした。笑えます。ほっとします。癒されます。よくしゃべる！NARU/女性/20代

        if( cnt == DISP_NUM ):
            f.write( body )

        f.write( code )
        cnt += 1

    # Wrtie html footer
    f.write( footer )

    f.close()


def sort_ch_data( list ):

    new_list = []
    print( len(list) )

    # 3列目でソート
    list.sort(key=lambda x: x[2])

    return list
    # Pickup data
    for li in list:
        rank = li[2]
        if( rank != "" ):
            rank = int( rank )
            print( li )
            # new_list[ rank ] = li
    # os.makedirs( OUT_DIR, exist_ok=True)
    # # 禁止文字を削除
    # ch = re.sub(r'[\\/:*?"<>|]+','',ch)
    # filename = OUT_DIR + ch + ".html"
    # print( filename )


# ---------------
#  Main
# ---------------
if __name__ == '__main__':
    args = sys.argv

    if( len(args) < 2 ):
        print( "Error" )
    else:
        INFILE = args[1]
        # INFILE = "鳥さん系YouTubeチャンネル人気投票 _決定版（回答_集計0419 - 集計_0419.csv"

        list = []
        # Read CSV data
        with open(INFILE, encoding="utf_8_sig") as f:
            reader = csv.reader(f)
            next(reader)   # Skip 1st line
            for row in reader:
                ch    = row[0]
                id    = row[1]
                rank  = row[2]
                cm    = row[3]
                age   = row[4]
                sex   = row[5]
                name  = row[6]

                # Cleaning
                if( rank == "" ) :
                    rank = 10000
                else:
                    rank = int(rank)
                # if( age == "" ) : age = "--"
                # if( sex == "" ) : sex = "--"
                # if( name == "" ) : name = "匿名さん"

                # IDが空欄の場合は無効コメント
                if( (id != "") and (cm != "") ):
                    list.append( [ch, id, rank, cm, name, sex, age] )
        # print( list )

        pre_id = ""
        # ch_name = ""
        data = []
        init = True
        for li in list:
            ch, id, rank, cm, name, sex, age = li
            if( init ):
                data    = []   # Clear
                pre_id  = id   # Save
                init    = False
            if( (id != pre_id) ):
                # 3列目でソート
                data.sort(key=lambda x: x[2])
                ch_name = data[0][0]
                export_html( ch_name, data )
                data    = []   # Clear
                pre_id  = id   # Save

            data.append( li )

        # 最後は自分で出力する
        # 3列目でソート
        data.sort(key=lambda x: x[2])
        ch_name = data[0][0]
        export_html( ch_name, data )
        data    = []   # Clear
        pre_id  = id   # Save
