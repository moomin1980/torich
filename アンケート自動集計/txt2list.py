# coding: utf-8

# -------------
# HTML list code generator
# Usage : Drag & Drop a text file

import sys
import os

header = \
    '<!doctype html>\n'\
    '<html lang="ja">\n'\
    '<head>\n'\
    '<meta charset="UTF-8">\n'\
    '<title>txt2list</title>\n'\
    '</head>\n'\
    '<body>\n'\
    '  <ul>\n'

footer = \
    '  </ul>\n'\
    '</body>\n'\
    '</html>'

def txt2list( InFile ):

    f = open( InFile, 'r')
    lines = f.readlines()
    f.close()

    filename = os.path.splitext( InFile )[0]
    OutFile = filename + '.html'
    # print( OutFile )
    f = open( OutFile, 'w')

    # Write html header
    f.write( header )

    cnt = 0
    for line in lines:
        text = line.replace('\n', '')
        text = text.replace('\r', '')   # for windows

        if( len(text) > 0 ):
            cnt += 1
            print( '[' + str(cnt) + '] ' + text)
            f.write( "    <li><span>" + text + "</span></li>\n")

    # Wrtie html footer
    f.write( footer )

    f.close()

if __name__ == '__main__':
    args = sys.argv
    if 1 < len(args):
        files = args
        for file in files:
            if ( os.path.isfile( file ) ):
                filename, ext = os.path.splitext( file )
                if( ext == '.txt' ):
                    print( 'Input file --> ' + str( file ) )
                    # Call main function
                    txt2list( file )
            else:
                print('[ERROR] 対象のファイルが存在しません\n')
    else:
        print('[ERROR] テキストファイルを引数として入力してください\n')
        print('< 使い方 >')
        print('python txt2list.py sample.txt\n')
        print('< もしくは (全てのテキストファイルが対象) >')
        print('python txt2list.py *\n')
